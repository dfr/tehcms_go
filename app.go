package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/coopernurse/gorp"
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"strconv"
)

var (
	Dbh       *gorp.DbMap
	templates = map[string]*template.Template{
		"index":        template.Must(template.New("main.html").Delims("[%", "%]").ParseFiles("./views/main.html", "./views/index.html")),
		"view_product": template.Must(template.New("main.html").Delims("[%", "%]").ParseFiles("./views/main.html", "./views/viewProduct.html")),
		"cart":         template.Must(template.New("main.html").Delims("[%", "%]").ParseFiles("./views/main.html", "./views/cart.html")),
		"admin_index":  template.Must(template.New("index.html").Delims("[%", "%]").ParseFiles("./views/admin/index.html")),
	}
)

/*
type AppServ struct {
	Router *httprouter.Router
}

func (s AppServ) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Router.ServeHTTP(w, r)
}
*/

//
// Validation
//
type ValidationError struct {
	Property string `json:"property"`
	Message  string `json:"message"`
}

type ValidationResult struct {
	Errors []ValidationError
}

func (v *ValidationResult) hasErrors() bool {
	return len(v.Errors) > 0
}

func (v *ValidationResult) errorsAsJson() []byte {
	jsonBytes, _ := json.Marshal(map[string]interface{}{"errors": v.Errors})
	return jsonBytes
}

type Validable interface {
	Validate() ValidationResult
}

//
// Controllers
//

type PageData struct {
	Categories    []*Category
	Products      []*Product
	Product       *Product
	CategoryAttrs []*FilterAttr
	SelectedCat   *Category
	Title         string
	LoggedIn      bool
	Login         string
	Req           url.Values
	Cart          *Cart
}

type FilterAttr struct {
	CatAttr *CategoryAttr
	Options []*FilterAttrOpt
}

type FilterAttrOpt struct {
	K string
	V string
}

func indexPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	data := PageData{}
	data.Categories, _ = fetchAllCategories()
	data.Products, _ = fetchRecentProducts(10)
	data.Title = "Index"
	execTemplate("index", w, data)
}

func listProductsPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	data := PageData{}
	data.Title = "Products for category"
	data.Categories, _ = fetchAllCategories()
	catId, err1 := strconv.ParseInt(r.URL.Query().Get("category"), 10, 64)
	prods, err2 := fetchProductsByParams(r.URL.Query())
	if !notifyError(w, err1) && !notifyError(w, err2) {
		var catattrs []*CategoryAttr
		Dbh.Select(&catattrs, "SELECT * FROM category_attrs WHERE Category=?", catId)
		data.CategoryAttrs = prepareFilterData(catattrs)
		data.SelectedCat = findCatInSlice(catId, data.Categories)
		data.Products = prods
		data.Req = r.URL.Query()
	}
	execTemplate("index", w, data)
}

func viewProductPage(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	idStr := ps.ByName("id")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if !notifyError(w, err) {
		prod, _ := fetchProductById(id)
		data := PageData{Product: prod}
		data.Categories, _ = fetchAllCategories()
		data.SelectedCat = findCatInSlice(prod.Category, data.Categories)
		execTemplate("view_product", w, data)
	}
}

func adminIndexPage(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var data struct {
		Statuses    []string
		SearchTypes []string
	}
	data.SearchTypes = []string{"multiselect", "select", "text", "range", "checkbox", "none"}
	data.Statuses = []string{"unconfirmed", "confirmed", "blocked"}
	execTemplate("admin_index", w, data)
}

func extByMime(filetype string) string {
	switch filetype {
	case "image/jpeg", "image/jpg":
		return "jpg"
	case "image/gif":
		return "gif"
	case "image/png":
		return "png"
	case "application/pdf":
		return "pdf"
	default:
		return ""
	}
}

func initDB(dbFile string) *gorp.DbMap {
	db, err := sql.Open("sqlite3", dbFile)
	errFatal(err, "sql.Open failed")
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.SqliteDialect{}}

	//
	dbmap.TraceOn("[gorp]", log.New(os.Stdout, "myapp:", log.Lmicroseconds))

	// create tables
	dbmap.AddTableWithName(Category{}, "categories").SetKeys(true, "Id").ColMap("Title").SetNotNull(true)
	tProduct := dbmap.AddTableWithName(Product{}, "products").SetKeys(true, "Id")
	tProduct.ColMap("Title").SetNotNull(true)
	tProduct.ColMap("Category").SetNotNull(true)

	dbmap.AddTableWithName(CategoryAttr{}, "category_attrs").SetKeys(false, "Attr", "Category")

	dbmap.AddTableWithName(ProductAttrVal{}, "product_attrvals").SetKeys(false, "Attr", "Product")

	dbmap.AddTableWithName(Article{}, "articles").SetKeys(true, "Id")

	dbmap.AddTableWithName(ArticleTag{}, "article_tags").SetKeys(false, "Name")

	tUsers := dbmap.AddTableWithName(User{}, "users").SetKeys(true, "Id")
	tUsers.ColMap("Name").SetNotNull(true)
	tUsers.ColMap("Login").SetUnique(true).SetNotNull(true)
	tUsers.ColMap("Email").SetUnique(true)

	dbmap.AddTableWithName(UserGroup{}, "user_groups").SetKeys(false, "Name").ColMap("Name").SetNotNull(true)

	err = dbmap.CreateTablesIfNotExists()
	errFatal(err, "Create tables failed")

	dbmap.Insert(&UserGroup{"admins"}, &UserGroup{"users"})

	return dbmap
}

func initRoutes() *httprouter.Router {
	// Site routes
	router := httprouter.New()
	router.ServeFiles("/public/*filepath", http.Dir("./public"))
	router.GET("/", indexPage)
	router.GET("/list_products", listProductsPage)
	router.GET("/view_product/:id", viewProductPage)
	router.GET("/cart", cartPage)
	router.GET("/admin", adminIndexPage)

	// Categories
	router.GET("/api/categories", apiCategories)
	router.POST("/api/categories", apiSaveCategory)
	router.POST("/api/categories/:id", apiSaveCategory)
	router.DELETE("/api/categories/:id", apiDeleteCategory)

	// Products
	router.GET("/api/products", apiProducts)
	router.POST("/api/products", apiSaveProduct)
	router.POST("/api/product/upload", apiUploadProductFile)
	router.DELETE("/api/product/rmfile/:filename", apiRemoveProductFile)
	router.POST("/api/products/:id", apiSaveProduct)
	router.DELETE("/api/products/:id", apiDeleteProduct)
	router.GET("/api/products/:id", apiLoadProduct)
	router.GET("/api/product_total", apiCountProducts)
	router.GET("/api/productattrs", apiProductAttrs)

	// Articles
	router.GET("/api/articles", apiArticles)
	router.POST("/api/articles", apiSaveArticle)
	router.POST("/api/articles/:id", apiSaveArticle)
	router.DELETE("/api/articles/:id", apiDeleteArticle)
	router.GET("/api/articles/:id", apiLoadArticle)
	router.GET("/api/articletotal", apiCountArticles)
	router.GET("/api/articletags", apiArticleTags)

	// Users
	router.GET("/api/users", apiUsers)
	router.POST("/api/users", apiSaveUser)
	router.POST("/api/users/:id", apiSaveUser)
	router.DELETE("/api/users/:id", apiDeleteUser)
	router.GET("/api/users/:id", apiLoadUser)
	router.GET("/api/usertotal", apiCountUsers)
	router.GET("/api/usergroups", apiUserGroups)

	return router
}

func errFatal(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}

func notifyError(w http.ResponseWriter, err error) bool {
	if err == nil {
		return false
	} else {
		_, file, line, _ := runtime.Caller(1)
		log.Printf("%s:%d %s\n", file, line, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return true
	}
}

func execTemplate(name string, w http.ResponseWriter, data interface{}) error {
	err := templates[name].ExecuteTemplate(w, "main", data)
	//err := templates[name].Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return err
	}
	return nil
}

func stringifyTemplate(t *template.Template, data interface{}) string {
	var doc bytes.Buffer
	t.ExecuteTemplate(&doc, "main", data)
	return doc.String()
}

func genericValidateJsonRequest(w http.ResponseWriter, r *http.Request, obj Validable) error {
	w.Header().Set("Content-Type", "application/json")
	err := json.NewDecoder(r.Body).Decode(&obj)
	if notifyError(w, err) {
		return err
	}
	validateResult := obj.Validate()
	if validateResult.hasErrors() {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(validateResult.errorsAsJson())
		return fmt.Errorf("validation error")
	} else {
		return nil
	}
}

func genericJsonResponse(w http.ResponseWriter, v interface{}) {
	jsonBytes, err := json.Marshal(v)
	//log.Println(string(jsonBytes))
	if notifyError(w, err) {
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonBytes)
}

func getSortLimitParts(q url.Values) (string, string) {
	var orderBy string = " ORDER BY Id DESC "
	var limit string
	if l, err := strconv.Atoi(q.Get("limit")); err != nil && l > 0 {
		limit = "LIMIT " + strconv.Itoa(l)
	}
	if l, err := strconv.Atoi(q.Get("offset")); limit != "" && err != nil {
		limit += ", " + strconv.Itoa(l)
	}
	return orderBy, limit
}

func findCatInSlice(catId int64, cats []*Category) *Category {
	for i := range cats {
		if cats[i].Id == catId {
			return cats[i]
		}
	}
	return nil
}

func prepareFilterData(attrs []*CategoryAttr) []*FilterAttr {
	fattrs := make([]*FilterAttr, 0, len(attrs))
	for i, _ := range attrs {
		if attrs[i].SearchType == "select" || attrs[i].SearchType == "multiselect" {
			var fopts []*FilterAttrOpt
			Dbh.Select(&fopts, "SELECT Value K, Value V FROM product_attrvals WHERE Attr=? GROUP BY Value", attrs[i].Attr)
			fattrs = append(fattrs, &FilterAttr{attrs[i], fopts})
		} else {
			fattrs = append(fattrs, &FilterAttr{attrs[i], nil})
		}
	}
	return fattrs
}

func main() {
	Dbh = initDB("/tmp/tehdb.bin")
	router := initRoutes()

	//app := AppServ{Router: router}
	//log.Fatal(http.ListenAndServe(":3001", app))

	log.Fatal(http.ListenAndServe(":3001", context.ClearHandler(router)))
}
