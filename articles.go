package main

import (
	"bytes"
	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type Article struct {
	Id     int64        `json:"id,string"`
	Title  string       `json:"title"`
	Added  int64        `json:"added,string"`
	Text   string       `json:"text"`
	Tagstr string       `json:"-"`
	Tags   []ArticleTag `json:"tags" db:"-"`
}

type ArticleTag struct {
	Name string `json:"name"`
}

// Validation

func (p *Article) Validate() ValidationResult {
	var r ValidationResult
	if p.Title == "" {
		r.Errors = append(r.Errors, ValidationError{"title", "empty"})
	}
	if p.Text == "" {
		r.Errors = append(r.Errors, ValidationError{"text", "empty"})
	}
	return r
}

//
// Controllers
//

func getArticleWherePart(q url.Values) (string, map[string]interface{}) {
	var where []string
	var whereStr string
	binds := make(map[string]interface{})
	if tags := q.Get("tags"); tags != "" {
		where = append(where, "Tagstr LIKE :tag")
		binds["tag"] = "%" + tags + "%"
	}
	if keyword := q.Get("keyword"); keyword != "" {
		where = append(where, "(Title LIKE :kw OR Text LIKE :kw)")
		binds["kw"] = "%" + keyword + "%"
	}

	if len(where) > 0 {
		whereStr = "WHERE " + strings.Join(where, " AND ")
	}
	return whereStr, binds
}

func apiArticles(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	whereStr, binds := getArticleWherePart(r.URL.Query())
	orderBy, limit := getSortLimitParts(r.URL.Query())
	var artlist []*Article
	Dbh.Select(&artlist, "SELECT * FROM articles "+whereStr+orderBy+limit, binds)
	for i, _ := range artlist {
		artlist[i].Tags = tagstrToArticleTags(artlist[i].Tagstr)
	}
	genericJsonResponse(w, artlist)
}

func apiCountArticles(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	whereStr, binds := getArticleWherePart(r.URL.Query())
	count, _ := Dbh.SelectStr("SELECT COUNT(*) FROM articles "+whereStr, binds)
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(count))
}

func apiLoadArticle(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	idStr := ps.ByName("id")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if !notifyError(w, err) {
		var article *Article
		err = Dbh.SelectOne(&article, "SELECT * FROM articles WHERE Id=?", id)
		if !notifyError(w, err) {
			// tags
			article.Tags = tagstrToArticleTags(article.Tagstr)
			log.Printf("%#v\n", article.Tags)
			genericJsonResponse(w, article)
		}
	}
}

func apiSaveArticle(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	trans, err := Dbh.Begin()
	if notifyError(w, err) {
		return
	}
	article := Article{}
	err = genericValidateJsonRequest(w, r, &article)
	if err == nil {
		article.Tagstr = articleTagsToTagstr(article.Tags, "\n")
		if article.Id > 0 {
			_, err = trans.Update(&article)
		} else {
			err = trans.Insert(&article)
		}
		if notifyError(w, err) {
			return
		}
		// save tags
		for _, tag := range article.Tags {
			trans.Insert(&tag) // ignore duplicate key errors
		}
		err = trans.Commit()
		if notifyError(w, err) {
			return
		}
		w.Write([]byte(`{"ok":1}`))
	}
}

func apiDeleteArticle(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.ParseInt(ps.ByName("id"), 10, 64)
	if !notifyError(w, err) {
		var article Article
		article.Id = id
		_, err := Dbh.Delete(&article)
		if !notifyError(w, err) {
			w.Write([]byte(`{"ok":1}`))
		}
	}

}

func apiArticleTags(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var items []*ArticleTag
	Dbh.Select(&items, "SELECT * FROM article_tags")
	genericJsonResponse(w, items)
}

func tagstrToArticleTags(tagstr string) []ArticleTag {
	tags := strings.FieldsFunc(tagstr, func(n rune) bool { return n == '\n' })
	articleTags := make([]ArticleTag, 0, len(tags))
	for i, _ := range tags {
		articleTags = append(articleTags, ArticleTag{tags[i]})
	}
	return articleTags
}

func articleTagsToTagstr(tags []ArticleTag, sep string) string {
	var buffer bytes.Buffer
	for i, _ := range tags {
		buffer.WriteString(tags[i].Name)
		if i < len(tags) {
			buffer.WriteString(sep)
		}
	}
	return buffer.String()
}
