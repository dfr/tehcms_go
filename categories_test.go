package main

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func init() {
	dbFile := "/tmp/tehdb_test.bin"
	os.Create(dbFile)
	Dbh = initDB(dbFile)
}

func TestIdToPath(t *testing.T) {
	assert.Equal(t, "", idToPath(0))
	assert.Equal(t, "002s", idToPath(100))
	assert.Equal(t, "zzzz", idToPath(1679615))
}

func TestApiEmptyCategories(t *testing.T) {
	hf := func(w http.ResponseWriter, r *http.Request) {
		apiCategories(w, r, nil)
	}
	handler := http.HandlerFunc(hf)
	recorder := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "", nil)
	assert.Nil(t, err)

	handler.ServeHTTP(recorder, req)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.HeaderMap.Get("Content-Type"))
	assert.Equal(t, "[]", recorder.Body.String())
}

func TestApiSaveCategory(t *testing.T) {
	hf := func(w http.ResponseWriter, r *http.Request) {
		apiSaveCategory(w, r, nil)
	}
	handler := http.HandlerFunc(hf)
	recorder := httptest.NewRecorder()
	reqBody := strings.NewReader(`{"parent":"0","title":"Холодильники","description":"тест"}`)
	req, err := http.NewRequest("POST", "/api/categories", reqBody)
	assert.Nil(t, err)

	handler.ServeHTTP(recorder, req)

	assert.Equal(t, 200, recorder.Code)
	assert.Equal(t, "application/json", recorder.HeaderMap.Get("Content-Type"))
	assert.Equal(t, `{"ok":1}`, recorder.Body.String())
	count, _ := Dbh.SelectInt("SELECT COUNT(*) FROM categories")
	assert.Equal(t, 1, count)

}
