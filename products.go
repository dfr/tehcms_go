package main

import (
	"database/sql"
	"encoding/json"
	"github.com/cznic/fileutil"
	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
	"github.com/nfnt/resize"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

const (
	UploadPath = "./public/upload/"
)

type Product struct {
	Id          int64            `json:"id,string"`
	Title       string           `json:"title"`
	Category    int64            `json:"category,string"`
	Description string           `json:"description"`
	Price       int64            `json:"price,string"`
	Price1      int64            `json:"price1"`
	Price2      int64            `json:"price2"`
	Imagestr    string           `json:"-"`
	Images      []string         `json:"images" db:"-"`
	AttrVals    []ProductAttrVal `json:"attrs" db:"-"`
}

type ProductAtrrValView struct {
	Product
	Attr  sql.NullString
	Value sql.NullString
}

type ProductAttrVal struct {
	Attr    string `json:"attr"`
	Product int64  `json:"product,string"`
	Value   string `json:"value"`
}

func (p *Product) Validate() ValidationResult {
	var r ValidationResult
	if p.Title == "" {
		r.Errors = append(r.Errors, ValidationError{"title", "empty"})
	}
	if p.Category == 0 {
		r.Errors = append(r.Errors, ValidationError{"category", "empty"})
	}
	for i, attrVal := range p.AttrVals {
		if attrVal.Value == "" {
			r.Errors = append(r.Errors, ValidationError{"attrs[" + strconv.Itoa(i) + "].value", "empty"})
		}
		if attrVal.Attr == "" {
			r.Errors = append(r.Errors, ValidationError{"attrs[" + strconv.Itoa(i) + "].attr", "empty"})
		}
	}
	return r
}

func postProcessProductlist(prodlist []*Product) {
	for i, _ := range prodlist {
		prodlist[i].Images = strings.FieldsFunc(prodlist[i].Imagestr, func(n rune) bool { return n == '\n' })
	}
}

func fetchRecentProducts(limit int) ([]*Product, error) {
	var prodlist []*Product
	_, err := Dbh.Select(&prodlist, "SELECT * FROM products ORDER BY Id Desc LIMIT ?", limit)
	postProcessProductlist(prodlist)
	return prodlist, err
}

func fetchProductsByCategory(catId int64, limit int) ([]*Product, error) {
	var prodlist []*Product
	_, err := Dbh.Select(&prodlist, "SELECT * FROM products WHERE Category = ? ORDER BY Id Desc LIMIT ?", catId, limit)
	postProcessProductlist(prodlist)
	return prodlist, err
}

func fetchProductsByParams(q url.Values) ([]*Product, error) {
	// fetch and prepare cat attrs
	var catAttrsS []*CategoryAttr
	catAttrs := make(map[string]*CategoryAttr)
	Dbh.Select(&catAttrsS, "SELECT * FROM category_attrs")
	for _, ca := range catAttrsS {
		catAttrs[ca.Attr] = ca
	}

	// fetch products
	var prodlist []*Product
	limit := "20"
	binds := make([]interface{}, 0)
	var whereParts []string
	joinC := 1
	var joinAttrs [][]string
	for key := range q {
		attr, okAttr := catAttrs[key]
		if vals, ok := q[key]; ok {
			if key == "category" {
				whereParts = append(whereParts, "p.Category=?")
				binds = append(binds, vals[0])
			}
			if !okAttr {
				continue
			}
			switch attr.SearchType {
			case "checkbox":
				whereParts = append(whereParts, "pa"+strconv.Itoa(joinC)+".Value")
				joinAttrs = append(joinAttrs, []string{strconv.Itoa(joinC), attr.Attr})
				joinC += 1
			case "range":
				if vals[0] != "" || vals[1] != "" {
					var rangeParts []string
					if vals[0] != "" {
						rangeParts = append(rangeParts, "pa"+strconv.Itoa(joinC)+".Value >=?")
						binds = append(binds, vals[0])
					}
					if vals[1] != "" {
						rangeParts = append(rangeParts, "pa"+strconv.Itoa(joinC)+".Value <=?")
						binds = append(binds, vals[1])
					}
					whereParts = append(whereParts, "("+strings.Join(rangeParts, " AND ")+")")
					joinAttrs = append(joinAttrs, []string{strconv.Itoa(joinC), attr.Attr})
					joinC += 1
				}
			case "text", "select":
				whereParts = append(whereParts, "pa"+strconv.Itoa(joinC)+".Value=?")
				binds = append(binds, vals[0])
				joinAttrs = append(joinAttrs, []string{strconv.Itoa(joinC), attr.Attr})
				joinC += 1
			case "multiselect":
				var msParts []string
				for _, v := range vals {
					msParts = append(msParts, "pa"+strconv.Itoa(joinC)+".Value=?")
					binds = append(binds, v)
				}
				if len(msParts) > 0 {
					whereParts = append(whereParts, "("+strings.Join(msParts, " OR ")+")")
					joinAttrs = append(joinAttrs, []string{strconv.Itoa(joinC), attr.Attr})
					joinC += 1
				}
			}

		}
	}
	whereStr := strings.Join(whereParts, " AND ")
	joinBinds := make([]interface{}, 0)
	joinStr := ""
	for _, jattr := range joinAttrs {
		joinStr += " JOIN product_attrvals pa" + jattr[0] + " ON (p.Id=pa" + jattr[0] + ".Product AND pa" + jattr[0] + ".Attr = ?) "
		joinBinds = append(joinBinds, jattr[1])
	}

	binds = append(joinBinds, binds...)
	Dbh.Select(&prodlist, "SELECT * FROM products p "+joinStr+" WHERE "+whereStr+" ORDER BY p.Id DESC LIMIT "+limit, binds...)
	return prodlist, nil
}

func stringInSlice(a string, list []*string) bool {
	for _, b := range list {
		if *b == a {
			return true
		}
	}
	return false
}

func fetchProductById(id int64) (*Product, error) {
	var prod *Product
	prodViews := []ProductAtrrValView{}
	_, err := Dbh.Select(&prodViews, "SELECT p.*, a.Attr, a.Value FROM products p LEFT JOIN product_attrvals a ON a.Product=p.Id WHERE p.Id=? ORDER BY p.Id", id)
	if err != nil {
		return nil, err
	}
	// collapse join View to Product with Attrs
	for i, _ := range prodViews {
		pview := prodViews[i]
		if prod == nil {
			prod = &pview.Product
			prod.AttrVals = make([]ProductAttrVal, 0, len(prodViews))
			prod.Images = make([]string, 0)
		}
		if pview.Attr.Valid {
			prod.AttrVals = append(prod.AttrVals, ProductAttrVal{Attr: pview.Attr.String, Product: prod.Id, Value: pview.Value.String})
		}
	}
	// images
	prod.Images = strings.FieldsFunc(prod.Imagestr, func(n rune) bool { return n == '\n' })
	//log.Printf("%#v\n", prod.Images)
	return prod, nil
}

//
// Controllers
//

func apiProductAttrs(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var items []*struct {
		Attr string `json:"attr"`
	}
	Dbh.Select(&items, "SELECT Attr FROM product_attrvals")
	Dbh.Select(&items, "SELECT Attr FROM category_attrs")
	genericJsonResponse(w, items)
}

func getProductWherePart(q url.Values) (string, map[string]interface{}) {
	var where []string
	var whereStr string
	binds := make(map[string]interface{})
	if category := q.Get("category"); category != "" {
		where = append(where, "Category=:cat")
		binds["cat"] = category
	}
	if keyword := q.Get("keyword"); keyword != "" {
		where = append(where, "(Title LIKE :kw OR Description LIKE :kw)")
		binds["kw"] = "%" + keyword + "%"
	}

	if len(where) > 0 {
		whereStr = "WHERE " + strings.Join(where, " AND ")
	}
	return whereStr, binds
}

// http://localhost:3000/api/products?category=1&keyword=%D0%BA%D1%83&limit=20&offset=0&sortType=DESC
func apiProducts(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	whereStr, binds := getProductWherePart(r.URL.Query())
	orderBy, limit := getSortLimitParts(r.URL.Query())
	var prodlist []*Product
	Dbh.Select(&prodlist, "SELECT * FROM products "+whereStr+orderBy+limit, binds)
	genericJsonResponse(w, prodlist)
}

func apiCountProducts(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	whereStr, binds := getProductWherePart(r.URL.Query())
	count, _ := Dbh.SelectStr("SELECT COUNT(*) FROM products "+whereStr, binds)
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(count))
}

func apiLoadProduct(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	idStr := ps.ByName("id")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if !notifyError(w, err) {
		prod, err := fetchProductById(id)
		if !notifyError(w, err) {
			genericJsonResponse(w, prod)
		}
	}
}

func apiSaveProduct(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	trans, err := Dbh.Begin()
	if notifyError(w, err) {
		return
	}
	product := Product{}
	err = genericValidateJsonRequest(w, r, &product)
	if err == nil {
		product.Imagestr = strings.Join(product.Images, "\n")
		if product.Id > 0 {
			_, err = trans.Update(&product)
		} else {
			err = trans.Insert(&product)
		}
		if notifyError(w, err) {
			return
		}
		// save attrs
		trans.Exec("delete from product_attrvals where Product=?", product.Id)
		for _, attrVal := range product.AttrVals {
			err = trans.SelectOne(&attrVal, "select * from product_attrvals where Attr=? and Product=?", attrVal.Attr, product.Id)
			if err == sql.ErrNoRows {
				attrVal.Product = product.Id
				err = trans.Insert(&attrVal)
			} else {
				_, err = trans.Update(&attrVal)
			}
			if notifyError(w, err) {
				return
			}
		}
		err = trans.Commit()
		if notifyError(w, err) {
			return
		}
		w.Write([]byte(`{"ok":1}`))
	}
}

func apiUploadProductFile(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var err error
	files := make([]string, 0, 10)
	os.MkdirAll(UploadPath, 0755)
	if err = r.ParseMultipartForm(25000000); notifyError(w, err) {
		return
	}
	for _, fheaders := range r.MultipartForm.File {
		for _, hdr := range fheaders {
			var infile multipart.File
			var outfile *os.File
			var written int64
			// open uploaded
			if infile, err = hdr.Open(); notifyError(w, err) {
				return
			}
			// open destination
			ext := extByMime(hdr.Header.Get("Content-Type"))
			if ext == "" {
				http.Error(w, "Unknown file type", http.StatusBadRequest)
				return
			}
			// create tempfiles
			if outfile, err = fileutil.TempFile(UploadPath, "", "."+ext); notifyError(w, err) {
				return
			}
			// save file
			if written, err = io.Copy(outfile, infile); notifyError(w, err) {
				return
			}
			outfile.Close()
			_, outfileName := filepath.Split(outfile.Name())
			files = append(files, outfileName)
			err = mkThumbnail(outfile.Name(), ext)
			if notifyError(w, err) {
				return
			}
			log.Printf("uploaded file: %s ;mime:%+v;length:%d\n", outfileName, hdr.Header.Get("Content-Type"), int(written))
		}
	}
	b, err := json.Marshal(map[string]interface{}{"files": files})
	if !notifyError(w, err) {
		w.Write(b)
	}
}

func apiRemoveProductFile(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	filename := ps.ByName("filename")
	os.Remove(UploadPath + filename)
	os.Remove(UploadPath + "thumb_" + filename)
	w.Write([]byte(`{"ok":1}`))
}

func apiDeleteProduct(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	trans, err := Dbh.Begin()
	if notifyError(w, err) {
		return
	}
	id, err := strconv.ParseInt(ps.ByName("id"), 10, 64)
	if !notifyError(w, err) {
		var prod Product
		err := trans.SelectOne(&prod, "SELECT * FROM products p WHERE p.Id=?", id)
		_, err1 := trans.Delete(&prod)
		if !notifyError(w, err) && !notifyError(w, err1) {
			trans.Exec("delete from product_attrvals where Product=?", id)
			prod.Images = strings.FieldsFunc(prod.Imagestr, func(n rune) bool { return n == '\n' })
			for _, f := range prod.Images {
				os.Remove(UploadPath + f)
				os.Remove(UploadPath + "thumb_" + f)
			}
			trans.Commit()
		}
	}
	w.Write([]byte(`{"ok":1}`))
}

func mkThumbnail(origFileName string, ext string) error {
	reader, err := os.Open(origFileName)
	outDirName, outFileName := filepath.Split(origFileName)
	if err != nil {
		return err
	}
	defer reader.Close()
	// create thumbnail
	img, _, err := image.Decode(reader)
	if err != nil {
		return err
	}
	newImg := resize.Thumbnail(450, 450, img, resize.Bicubic)
	outfileThumb, err := os.Create(outDirName + "thumb_" + outFileName)
	if err != nil {
		return err
	}
	// save thumb
	switch ext {
	case "jpg":
		jpeg.Encode(outfileThumb, newImg, nil)
	case "gif":
		gif.Encode(outfileThumb, newImg, nil)
	case "png":
		png.Encode(outfileThumb, newImg)
	}
	outfileThumb.Close()
	return nil
}
