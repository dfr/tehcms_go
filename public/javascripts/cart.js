//
// Jquery based cart, needs jquery and jquery cookie plugin
//
// Cart cookie format2: id1-price1-qty1:id2-price2-qty2

var g_cartCookieName = 'techcmsYourCart';
var g_compCookieName = 'techcmsYourComp';
var TOK_DELIM = ":";

function addToCart(id, price) {
	var items = parseCartCookieString(g_cartCookieName);
	//var newprice = cart_total(items) + parseFloat(price);
    var qty = items[id] ? items[id].qty + 1 : 1;
    items[id] = { qty: qty, price: price };
	var s = createCartCookieString(items);
	$.cookie(g_cartCookieName, s, { path: '/' });
	cartAlert();
	cartUpdated(items);
}

function updateCartItems(upItems) {
	var items = parseCartCookieString(g_cartCookieName);
	for (var i in upItems) {
        if(isNaN(upItems[i].qty)) upItems[i].qty = 0;
        if(upItems[i].qty > 0)
		    items[i] = upItems[i];
        else
            delete items[i];
	}
	var s = createCartCookieString(items);
	$.cookie(g_cartCookieName, s, { path: '/' });
	cartUpdated(items);
}

function refreshCart() {
	var items = parseCartCookieString(g_cartCookieName);
	cartUpdated(items);
}

function cartAlert() {
	$('<div class="alert alert-success">Товар добавлен в корзину!</div>')
			.appendTo($('#alertbox')).fadeIn('slow').animate( {
				opacity : 1.0 }, 3000).fadeOut('slow', function() {
				$(this).remove();
			});
}

function cartUpdated(items) {
    var qty = 0;
    for(var i in items) qty += items[i].qty;
    var total = cart_total(items);
	$('.cart-qty').text(qty);
	$('.cart-total').text(total);
	//$('#cart-box').show();
    if(qty > 0)
	    $('.cart-msg').text('В корзине товаров: ' + qty + ', на сумму: ' + total);
    else
	    $('.cart-msg').text('Корзина пуста');
}

function parseCartCookieString(cookieName) {
	var c = $.cookie(cookieName);
	if(!c) return {};
	var tokens = c.split(TOK_DELIM);
	var items = {};
	for ( var i = 1; i < tokens.length; i++) {
		var t = tokens[i];
		var item = t.split("-");
		items[item[0]] = { price: parseFloat(item[1]), qty: parseInt(item[2]) };
	}
	return items;
}

function createCartCookieString(items) {
	var sb = "";
	sb += TOK_DELIM;
	for(var k in items) {
		sb += k;
		sb += "-";
		sb += items[k].price;
		sb += "-";
		sb += items[k].qty;
		sb += TOK_DELIM;
	}
	// cut off final TOK_DELIM
	return sb.substr(0, sb.length - 1);
}

function emptyCart() {
	$.cookie(g_cartCookieName, null, { path: '/' });
	cartUpdated({});
}

function cart_total(items) {
    var total = 0.0;
    for(var i in items) total += items[i].price * items[i].qty;
    return total;
}

//
// Jquery based product comparison feature
//

// add or remove from comparison list
function addToCompare(cb, id) {
	var ids = parseCompCookieString(g_compCookieName);
	var newids = [];
	for(var i = 0; i < ids.length; i++) {
		if(ids[i] == id) {
			cb.checked = false;
			id = 0;
		}
		else {
			newids.push(ids[i]);
		}
	}
	if(id > 0) newids.push(id);
	s = createCompCookieString(newids);
	$.cookie(g_compCookieName, s, { path: '/' });
	compUpdated(id);
}

function compUpdated(id) {
	var str = "Товар добавлен к сравнению";
	if(id == 0) str = "Товар удален из сравнения";
	$('<div class="quick-alert">'+str+'</div>')
			.insertAfter($('#cart-box')).fadeIn('slow').animate( {
				opacity : 1.0 }, 3000).fadeOut('slow', function() {
				$(this).remove();
			});
}


function parseCompCookieString(cookieName) {
	var c = $.cookie(cookieName);
	if(!c) return [];
	var tokens = c.split(TOK_DELIM);
	return tokens;
}

function createCompCookieString(ids) {
	var sb = "";
	for(var i = 0; i < ids.length; i++) {
		sb += ids[i];
		sb += TOK_DELIM;
	}
	return sb.substr(0, sb.length - 1);
}

 

function roundNumber(number, digits) {
	var multiple = Math.pow(10, digits);
	var rndedNum = Math.round(number * multiple) / multiple;
	return rndedNum.toFixed(digits);
}
