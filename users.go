package main

import (
	"bytes"
	"github.com/coopernurse/gorp"
	"github.com/julienschmidt/httprouter"
	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type UserGroup struct {
	Name string `json:"name"`
}

type User struct {
	Id       int64       `json:"id,string"`
	Login    string      `json:"login"`
	Email    string      `json:"email"`
	Name     string      `json:"name"`
	Password string      `json:"password"`
	Groups   []UserGroup `json:"groups" db:"-"`
	Groupstr string      `json:"-"`
	Status   string      `json:"status"`
	Added    int64       `json:"added,string"`
}

// Validation

func (u *User) Validate() ValidationResult {
	var r ValidationResult
	if u.Login == "" {
		r.Errors = append(r.Errors, ValidationError{"login", "empty"})
	}
	return r
}

func (u *User) ValidateUniques(trans *gorp.Transaction) ValidationResult {
	var r ValidationResult
	uOld := User{}
	meCond := ""
	binds := map[string]interface{}{"login": u.Login, "email": u.Email, "id": u.Id}
	if u.Id > 0 {
		meCond = " AND Id <> :id"
	}
	err := trans.SelectOne(&uOld, "SELECT * FROM users WHERE Login=:login"+meCond, binds)
	if err == nil {
		r.Errors = append(r.Errors, ValidationError{"login", "duplicate"})
	}
	if u.Email != "" {
		err := trans.SelectOne(&uOld, "SELECT * FROM users WHERE Email=:email"+meCond, binds)
		if err == nil {
			r.Errors = append(r.Errors, ValidationError{"email", "duplicate"})
		}
	}
	return r
}

//
// Controllers
//

func getUserWherePart(q url.Values) (string, map[string]interface{}) {
	var where []string
	var whereStr string
	binds := make(map[string]interface{})
	if groups := q.Get("groups"); groups != "" {
		where = append(where, "Groupstr LIKE :group")
		binds["group"] = "%" + groups + "%"
	}
	if keyword := q.Get("keyword"); keyword != "" {
		where = append(where, "(Name LIKE :kw OR Login LIKE :kw OR Email LIKE :kw)")
		binds["kw"] = "%" + keyword + "%"
	}

	if len(where) > 0 {
		whereStr = "WHERE " + strings.Join(where, " AND ")
	}
	return whereStr, binds
}

func apiUsers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	whereStr, binds := getUserWherePart(r.URL.Query())
	orderBy, limit := getSortLimitParts(r.URL.Query())
	var userlist []*User
	Dbh.Select(&userlist, "SELECT * FROM users "+whereStr+orderBy+limit, binds)
	for i, _ := range userlist {
		userlist[i].Groups = groupstrToUserGroups(userlist[i].Groupstr, '\n')
	}
	genericJsonResponse(w, userlist)
}

func apiCountUsers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	whereStr, binds := getUserWherePart(r.URL.Query())
	count, _ := Dbh.SelectStr("SELECT COUNT(*) FROM users "+whereStr, binds)
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(count))
}

func apiLoadUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	idStr := ps.ByName("id")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if !notifyError(w, err) {
		var user *User
		err = Dbh.SelectOne(&user, "SELECT * FROM users WHERE Id=?", id)
		if !notifyError(w, err) {
			// groups
			user.Groups = groupstrToUserGroups(user.Groupstr, '\n')
			//log.Printf("%#v\n", user.Groups)
			genericJsonResponse(w, user)
		}
	}
}

func apiSaveUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	trans, err := Dbh.Begin()
	if notifyError(w, err) {
		return
	}
	defer trans.Commit()
	user := User{}
	err = genericValidateJsonRequest(w, r, &user)
	if err == nil {
		vr := user.ValidateUniques(trans)
		if vr.hasErrors() {
			w.WriteHeader(http.StatusBadRequest)
			w.Write(vr.errorsAsJson())
		} else {
			user.Groupstr = userGroupsToGroupstr(user.Groups, "\n")
			if user.Password != "" {
				// crypt password if not already crypted
				if _, err := bcrypt.Cost([]byte(user.Password)); err != nil {
					crypted, _ := bcrypt.GenerateFromPassword([]byte(user.Password), 5)
					user.Password = string(crypted)
				}
			}
			if user.Id > 0 {
				_, err = trans.Update(&user)
			} else {
				err = trans.Insert(&user)
			}
			if notifyError(w, err) {
				return
			}
			w.Write([]byte(`{"ok":1}`))
		}
	}
}

func apiDeleteUser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.ParseInt(ps.ByName("id"), 10, 64)
	if !notifyError(w, err) {
		var user User
		user.Id = id
		_, err := Dbh.Delete(&user)
		if !notifyError(w, err) {
			w.Write([]byte(`{"ok":1}`))
		}
	}

}

func apiUserGroups(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var items []*UserGroup
	Dbh.Select(&items, "SELECT * FROM user_groups")
	genericJsonResponse(w, items)
}

func groupstrToUserGroups(groupstr string, sep rune) []UserGroup {
	groups := strings.FieldsFunc(groupstr, func(n rune) bool { return n == sep })
	userGroups := make([]UserGroup, 0, len(groups))
	for i, _ := range groups {
		userGroups = append(userGroups, UserGroup{groups[i]})
	}
	return userGroups
}

func userGroupsToGroupstr(groups []UserGroup, sep string) string {
	var buffer bytes.Buffer
	for i, _ := range groups {
		buffer.WriteString(groups[i].Name)
		if i < len(groups) {
			buffer.WriteString(sep)
		}
	}
	return buffer.String()
}
